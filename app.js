const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const history = require('connect-history-api-fallback');
const constants = require('./src/constants');
const winstonConfigurer = require('./src/config/winston.configurer');

const app = express();

configureReqIdMiddleware(app);
configureMorgan(app);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

winstonConfigurer.configure();
configureRoutes(app);
configureStatic(app);
configureSpa(app);

module.exports = app;

function configureReqIdMiddleware(app) {
    const uuid = require('uuid');
    const httpContext = require('express-http-context');
    app.use(httpContext.middleware);
    app.use(function (req, res, next) {
        httpContext.set('reqId', uuid.v4());
        next();
    });
}

function configureMorgan(app) {
    const morgan = require('morgan');
    const httpContext = require('express-http-context');
    morgan.token('reqId', (req, res) => { return httpContext.get('reqId'); })
    const formatterMorgan = morgan(':reqId :remote-addr - :remote-user [:date[iso]] ":method :url HTTP/:http-version" :status :res[content-length] B :response-time ms');
    app.use(formatterMorgan);
}

function configureRoutes(app) {
    const rootRouter = require('./src/routes/root.route');
    app.use(constants.rootUrl, rootRouter);
    const versionRouter = require('./src/routes/version.route');
    app.use(constants.baseRest + '/version', versionRouter);
    const fortuneRouter = require('./src/routes/fortune.route');
    app.use(constants.baseRest + '/fortune', fortuneRouter);
}

function configureStatic(app) {
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(constants.baseWeb, express.static(path.join(__dirname, 'front/dist')));
}

function configureSpa(app) {
    const winston = require('winston');
    app.use(history({
        index: constants.baseWeb + '/index.html',
        logger: (message, ...optionalParams) => {
            winston.debug(message, optionalParams)
        },
    }));
    // https://github.com/bripkens/connect-history-api-fallback/blob/master/examples/static-files-and-index-rewrite/README.md#configuring-the-middleware
    configureStatic(app);
}