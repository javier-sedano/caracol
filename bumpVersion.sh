#!/bin/bash

pushd "$(dirname "$0")"

CURRENT_LINE=`grep '"version": ".*"' package.json`
CURRENT_VERSION=`echo $CURRENT_LINE | cut -d: -f2 | cut -d\" -f2`
CURRENT_MAJOR=`echo $CURRENT_VERSION | cut -d. -f1`
CURRENT_MINOR=`echo $CURRENT_VERSION | cut -d. -f2`
CURRENT_PATCH=`echo $CURRENT_VERSION | cut -d. -f3`

NEW_MINOR=$((CURRENT_MINOR+1))
NEW_VERSION=$CURRENT_MAJOR.$NEW_MINOR.0

echo "$CURRENT_VERSION --> $NEW_VERSION"

NEW_LINE=`echo "$CURRENT_LINE" | sed s/$CURRENT_VERSION/$NEW_VERSION/`
CURRENT_LINE_NUMBER=`fgrep -n "$CURRENT_LINE" package.json | cut -d: -f1`
sed -i "${CURRENT_LINE_NUMBER}s/.*/$NEW_LINE/" package.json

popd
