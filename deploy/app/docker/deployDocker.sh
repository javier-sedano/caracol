#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/caracol/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=caracol" --filter "status=running" --quiet`
docker container stop caracol
docker container rm caracol
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 16m -p 5480:4001 --restart unless-stopped --name caracol $IMAGE
if [ "$containerId" ]
then
  docker container start caracol
fi
