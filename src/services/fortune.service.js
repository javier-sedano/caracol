const child_process = require('child_process');
const logger = require('winston');
const FORTUNE = process.env.FORTUNE || "fortune"
const MAX_N = 5;

function FortuneService() {
    this.getFortune = (n, successCallback, errorCallback) => {
        logger.debug("Generating %d fortunes", n);
        if (n <= 0) {
            errorCallback("Incorrect argument, n=" + n);
            return;
        }
        if (n > MAX_N) {
            errorCallback("Too many requests, n=" + n);
            return;
        }
        const fortunes = Array();
        this.recursiveGetFortune(n, successCallback, errorCallback, fortunes);
    };

    this.recursiveGetFortune = (n, successCallback, errorCallback, accumulatedFortunes) => {
        if (n === 0) {
            successCallback(accumulatedFortunes);
            return;
        }
        child_process.exec(FORTUNE, (err, stdout, stderr) => {
            if (err) {
                errorCallback(stderr);
                return;
            }
            accumulatedFortunes.push(stdout);
            this.recursiveGetFortune(n - 1, successCallback, errorCallback, accumulatedFortunes);
        });
    }
}

module.exports = new FortuneService();
