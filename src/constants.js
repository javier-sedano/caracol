module.exports = {
    rootUrl: "/",
    baseUrl: "/caracol",
    baseRest: "/caracol/rest",
    baseWeb: "/caracol/web",
};