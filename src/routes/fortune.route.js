const express = require('express');
const router = express.Router();
const fortuneService = require('../services/fortune.service');
const logger = require('winston');

router.get('/', (req, res, _next) => {
  const n = req.query.n || 1;
  fortuneService.getFortune(
    n,
    (fortunes) => {
      res.set('Cache-Control', 'no-store, no-cache, must-revalidate');
      res.send(fortunes);
    }, (stderr) => {
      logger.error(stderr);
      res.sendStatus(500);
    }
  );
});

module.exports = router;
