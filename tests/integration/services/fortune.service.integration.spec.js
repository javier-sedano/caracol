const winstonConfigurer = require('../../../src/config/winston.configurer');
const FORTUNE_SERVICE = '../../../src/services/fortune.service';
const testConstants = require('../../test.constants');

describe("fortune service integration test", () => {
    const OLD_ENV = process.env;

    beforeEach(() => {
        jest.resetModules();
        process.env = { ...OLD_ENV };
        winstonConfigurer.configure();
    });

    afterEach(() => {
        process.env = OLD_ENV;
    });

    it("should return one fortune", (done) => {
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            1,
            (fortunes) => {
                expect(fortunes.length).toEqual(1);
                expect(fortunes[0]).toBeDefined();
                done();
            },
            (stderr) => {
                done.fail(stderr);
            }
        );
    });

    it("should error on wrong fortune binary (bad environment)", (done) => {
        process.env.FORTUNE = "/non/existing/fortune";
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            1,
            (fortunes) => {
                done.fail(fortunes);
            },
            (stderr) => {
                expect(stderr).toBeDefined();
                done();
            }
        );
    });

    it("should error on wrong fortune binary (no environment)", (done) => {
        delete process.env.FORTUNE;
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            1,
            (fortunes) => {
                done.fail(fortunes);
            },
            (stderr) => {
                expect(stderr).toBeDefined();
                done();
            }
        );
    });

    it("should return several fortunes", (done) => {
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            testConstants.maxFortunes,
            (fortunes) => {
                expect(fortunes.length).toEqual(testConstants.maxFortunes);
                expect(fortunes[0]).toBeDefined();
                expect(fortunes[1]).toBeDefined();
                expect(fortunes[2]).toBeDefined();
                done();
            },
            (stderr) => {
                done.fail(stderr);
            }
        );
    });

    it("should error on too many fortunes", (done) => {
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            testConstants.maxFortunes + 1,
            (fortunes) => {
                done.fail(fortunes);
            },
            (stderr) => {
                expect(stderr).toBeDefined();
                done();
            }
        );
    });

    it("should error on too few fortunes", (done) => {
        const fortuneService = require(FORTUNE_SERVICE);
        fortuneService.getFortune(
            0,
            (fortunes) => {
                done.fail(fortunes);
            },
            (stderr) => {
                expect(stderr).toBeDefined();
                done();
            }
        );
    });

});
