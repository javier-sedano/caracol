const app = require('../../app');
const supertest = require('supertest');
const apiConstants = require('./api.constants');
const testConstants = require('../test.constants');

const url = apiConstants.baseRest + '/fortune';

describe("/fortune API test", function () {
    it("should return one fortune", function (done) {
        supertest(app).get(url)
            .expect(200)
            .expect('Cache-Control', 'no-store, no-cache, must-revalidate')
            .expect((res) => {
                expect(res.body.length).toEqual(1);
                expect(res.body[0]).toBeDefined();
            })
            .end(done);
    });

    it("should return two fortunes", function (done) {
        supertest(app).get(url)
            .query({ n: 2 })
            .expect(200)
            .expect('Cache-Control', 'no-store, no-cache, must-revalidate')
            .expect((res) => {
                expect(res.body.length).toEqual(2);
                expect(res.body[0]).toBeDefined();
                expect(res.body[1]).toBeDefined();
            })
            .end(done);
    });

    it("should not return zero fortunes", function (done) {
        supertest(app).get(url)
            .query({ n: 0 })
            .expect(500)
            .end(done);
    });

    it("should not return too many fortunes", function (done) {
        supertest(app).get(url)
            .query({ n: (testConstants.maxFortunes + 1) })
            .expect(500)
            .end(done);
    });

});
