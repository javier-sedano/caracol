const app = require('../../app');
const supertest = require('supertest');
const apiConstants = require('./api.constants');

const url = apiConstants.baseRest + '/version';

describe("/version API test", function () {
    it("should return a version", function (done) {
        supertest(app).get(url)
            .expect(200, "undefined")
            .end(done);
    });
});
