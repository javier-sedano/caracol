Reference architecture of public web application with mobile-first responsive design based on Node.js/Express and Vue with CI in Gitlab-CI and CD to Google Compute Engine.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/caracol/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/caracol/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.caracol&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.caracol)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.caracol)
</details>

Keywords: node.js, nodejs, node, express, jest, supertest, sonarqube, winston, morgan, vue, vscode, devcontainer, docker, yargs, nodemon, concurrently, w3css, fontawesome, giphy

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* NPM SCRIPTS will show the available tasks:
  * Run "dependenciesInit" to download dependencies
  * Run "start:dev" to start serving (while watching for changes in both frontend and backend)
    * Most common development tasks are configured as npm tasks
  * Browse http://localhost:4001/caracol/

Useful links:

* https://nodejs.org/en/
* https://nodejs.dev/introduction-to-nodejs
* https://www.npmjs.com/package/debug
* https://expressjs.com/
* https://github.com/yargs/yargs/
* https://github.com/visionmedia/supertest
* https://jasmine.github.io/
* https://docs.sonarqube.org/latest/
* https://github.com/istanbuljs/nyc
* https://github.com/winstonjs/winston
* https://github.com/expressjs/morgan
* https://github.com/uuidjs/uuid#readme
* https://github.com/skonves/express-http-context#readme
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://jestjs.io/
* https://nodemon.io/
* https://github.com/kimmobrunfeldt/concurrently#readme
* https://github.com/bripkens/connect-history-api-fallback
* https://github.com/bripkens/connect-history-api-fallback/blob/master/examples/static-files-and-index-rewrite/README.md#configuring-the-middleware
* https://www.w3schools.com/w3css/
* https://fontawesome.com/
* https://github.com/axios/axios
* https://github.com/kentcdodds/cross-env#readme
* https://developers.giphy.com/docs/api/endpoint/#random
